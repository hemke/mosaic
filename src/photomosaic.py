"""
Copyright 2012 Matthew Hemke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
                                    
This software Automatically generates photomosaics using a set of images and a
target image.

This works by generating a large database of image features and searching
through them using approximate nearest neighbors search provided by pyflann.
"""

#modeline for vim editors
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

import os
import numpy
import pyflann
import pygame
import pickle
import math

def findimages(path):
    """
    Searches for images that can be imported by pygame and generates a file
    that lists each image's location.

    Pases value of path to os.expanduser, so use a '~' to expand to home
    directory.
    """

    #image formats that can be loaded by pygame
    pygameimgfmts = ["jpg", "png", "gif", "bmp", "pcx", "tga", "tif", "lbm",
                    "pbm", "pgm", "ppm", "xpm"]

    imglistfd = open("imglist.txt", 'w')
    for files in os.walk(os.path.expanduser(path)):
        for filename in files[2]:

            #get file extension and convert to lowercase
            fileext = filename.lower().rsplit('.', 1)[-1]
            if fileext in pygameimgfmts:
                #we found an image we can use so write path to file
                imglistfd.write("{}\n".format(os.path.join(files[0], filename)))
    imglistfd.close()

def generatefeaturevector(imgsurface, featuresize=(3, 3),
                                writeintermediate=False):
    """
    Generates a feature vector for the image given at path.

    Returns "None" if image cannot be opened.

    PARAMS:

    imgsurface- a pygame.Surface image

    featuresize - a two tuple of the size of the image to be used to generate
    the feature vector. Defaults to 3x3.
    """

    numpixels = featuresize[0] * featuresize[1]

    
    #crop image to square
    cropped = cropsquare(imgsurface, writeintermediate)

    #scale image to featursize
    scaled = pygame.transform.scale(cropped, featuresize)
    if writeintermediate:
        pygame.image.save(scaled, 'lastscaled.jpg')

    #convert image to a 2d array to get direct access to pixels
    # creates 3d array with indices [x][y][0=red, 1=green, 2=blue]
    try:
        pixelarray = pygame.surfarray.array3d(scaled)
    except ValueError:
        return None
    return numpy.reshape(pixelarray, (numpixels * 3,))

def cropsquare(imgsurface, writeintermediate=False):
    """
    generates a pygame subsurface that contains the largest square portion of the
    image that still fits in the image.
    """

    #get size of loaded image
    [width, height] = imgsurface.get_size()

    if width > height:
        left = (width - height) / 2
        top = 0
        cropbox = pygame.Rect(left, top, height, height)
    elif width < height:
        left = 0
        top = (height - width) / 2
        cropbox = pygame.Rect(left, top, width, width)
    else:
        #image is square, cropbox is full image.
        cropbox = pygame.Rect(0, 0, width, height)
    cropped = imgsurface.subsurface(cropbox)
    if writeintermediate:
        pygame.image.save(cropped, 'lastcropped.jpg')

    return cropped
def indeximages(filename="imglist.txt"):
    """
    reads a list of images in from a file (one per line) and generates a FLANN
    index from all images in the file.

    if no filename is given, it will read from "imglist.txt" which is the
    default as created by findimages()
    """
    flannindex = pyflann.FLANN()
    featureindex = []
    imglookup = []
    with open(filename) as imglist:
        for image in imglist:
            try:
                imgsurface = pygame.image.load(image.rstrip())
            except pygame.error as err:
                print("Pygame Error:{} on image {}".format(err, image))
            vector = generatefeaturevector(imgsurface)
            if vector is not None:
                featureindex.append(vector)
                imglookup.append(image)
    npfeatureindex = numpy.array(featureindex)
    flannindex.build_index(npfeatureindex)
    #save flann index
    flannindex.save_index("flannindex.flidx")
    #save points used to generate flann index
    numpy.save('featurepts', npfeatureindex)
    #save imagelookup to correlate results from flannindex with the appropriate
    #image
    with open('imagelookup.pkl', 'wb') as lookuptable:
        pickle.dump(imglookup, lookuptable, pickle.HIGHEST_PROTOCOL)
class Mosaic:
    """
    Provides tools for automatically generating a photomosaic from a library
    images and a source image.
    """
    def __init__(self):
        self.mypyflann =  pyflann.FLANN()
        self.npfeatures = None
        self.imagelookup = None

    def loadindex(self, directory='index'):
        """
        Loads in index files from disk.

        Files as generated by index images will be loaded from the given
        directory or the directory 'index' by default.
        """
        #load feature vectors
        featureindexpath = os.path.join(directory, 'featurepts.npy')
        self.npfeatures = numpy.load(featureindexpath)

        #load flann index
        flannindexpath = os.path.join(directory, 'flannindex.flidx')
        self.mypyflann.load_index(flannindexpath, self.npfeatures)

        #load imagelookup
        imglookuppath = os.path.join(directory, 'imagelookup.pkl')
        with open(imglookuppath, 'rb') as ilpfd:
            self.imagelookup = pickle.load(ilpfd)
    def findmatch(self, featurevector):
        """
        Retruns the path to the nearest match of the image described by
        featurevector.
        """
        nearestneighbor = self.mypyflann.nn_index(featurevector)
        return self.imagelookup[int(nearestneighbor[0])].rstrip()
    def generatemosaic(self, imagepath, rows=20, tilesize=128):
        """
        Actually generates a mosaic in the likeness of the image located at
        imagepath.

        Params:

        imagepath - a string containing a path to an image.

        rows - the number of image tiles to be used in the vertical direction.
        Defaults to 20.

        tilesize - the size of the tiles of the final image in pixels.
        """

        #load target image
        original = pygame.image.load(imagepath)
        [imwidth, imheight] = original.get_size()

        #resample image to size
        factor = (3*rows)/(1.0 * imheight)
        newwidth = int(imwidth*factor)
        newheight = int(imheight*factor)
        print "resized to (%d,%d)" % (newwidth, newheight)
        resized = pygame.transform.scale(original, (newwidth, newheight))
        pygame.image.save(resized, 'resized.jpg')

        #initialize final image
        columns = math.ceil(newwidth/3.0)
        print "Rows: %d Columns: %d" % (rows, columns)
        finalsize = (columns*tilesize, rows*tilesize)
        finalimage = pygame.Surface(finalsize)

        # loop over 3x3 pixel blocks
        for i in range(newwidth/3):
            for j in range(newheight/3):
                cliprect = pygame.Rect(3*i, 3*j, 3, 3)
                clipsurf = resized.subsurface(cliprect)
                clipfeat = generatefeaturevector(clipsurf)

                matchpath = self.findmatch(clipfeat)
                #load matched image
                curtile = pygame.image.load(matchpath)
                
                #crop tile to square
                curtile = cropsquare(curtile)
                
                #resize to tilesize
                curtile = pygame.transform.smoothscale(curtile,
                                                (tilesize, tilesize))

                #blit onto final image
                leftedge = tilesize * i
                topedge = tilesize * j
                finalimage.blit(curtile, (leftedge, topedge))
        pygame.image.save(finalimage, "photomosaic.png")
