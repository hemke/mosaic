\documentclass[sort&compress, 3p, final, 12pt]{elsarticle} \usepackage{mciteplus}
\usepackage{url}
\usepackage{graphicx}
\usepackage{rotating}

\journal{ECE533 Final Project}
\title{Automatic Photo-mosaic Generation using K-D Tree Matching}
\author{Matthew Hemke}
\address{All files can be downloaded at:\\
            https://bitbucket.org/hemke/mosaic/src\\
            \copyright 2012 All Rights Reserved.}
\date{today}


\begin{document}
    \begin{abstract}
        In this paper I outline the steps taken to generate a photo-mosaic using
        only simple parameters such as image size and source images and then to
        automatically produce a tile mosaic in the likeness of the original
        image using other images as the pixels. I then discuss and detail the
        software developed and the methods it uses to perform this task.
    \end{abstract}
    \maketitle
    
    \section{Introduction}
        A number of challenges exist in the generation of a photo-mosaic, many
        of them stemming from the fact that the set of mosaic images, by
        necessity, must be large. Specifically, we must: store and rapidly
        recall a large number of mosaic images to be used as tiles for the
        completed mosaic, rapidly match features in the target image to
        features in the database of mosaic images, and match a multidimensional
        feature vector to a large database. Ideally, this database must be very
        large: if we replace pixels with image tiles, even the generation of a
        photo-mosaic from a relatively simple target image will require tens of
        thousands of database queries. Thus, to ensure reasonable speed to
        generate the image, database queries must be practically instantaneous
        to ensure rapid production of the image.

    \section{Approach}
        In this section, I outline the methods and approaches used to generate a
        photo-mosaic. For illustrative purposes, I will show intermediate steps
        using a sample image (Figure \ref{pizza}).
        
        \begin{figure}
            \includegraphics[width=\columnwidth]{images/pizza}
            \caption{Original image used in this section to demonstrate how
            images are processed to generate the database.}
            \label{pizza}
        \end{figure}

        \subsection{Tools}
            I decided to write the mosaic software using Python version 2.7.3. I
            chose Python because it is quick, free, and has a wide variety of
            useful libraries readily available. For this project, I used a
            handful of these supplemental libraries to facilitate coding. I chose
            to use Pygame (a python wrapper for the SDL library) for image
            processing functions such as crop, re-size, load, save, blit, etc.
            Because Pygame is a mature library intended for video game
            development it has stable and fast methods useful for this task.

            To assist in building the database of images required, I used python
            bindings to another library called flann\cite{muja_flann_2009} which
            provides tools for building and searching through high-dimensional
            spaces and for finding approximate nearest neighbors. Flann
            automatically chooses the best algorithm from kmeans, kd
            trees\cite{kdtree}, and others.

            The Numpy library was used to provide numerical and matrix operations.
        
        \subsection{Image Searching and Location}
            In order to automate the generation of the mosaic, a source of a
            large number of images must be compiled and categorized for the
            image database. To approach this, I wrote python function
            \verb~findimages~ that recursively walks through a directory
            locating any images that can be loaded, filtering by file extension to
            find common image formats such as \verb~.png~, \verb~.jpg~, and
            others. The list of images generated (about 4100 for the examples
            later in this report) are then saved as a text file with the path to
            each image on a line. Then, for image description and
            categorization, we simply read in the list of images and process
            them line by line.
        
        \subsection{Image Pre-Processing}
            Images for the mosaic are first pre-processed by cropping them so
            that they are square, and then down-sampling each to a small
            resolution, say $3\times3$. These image pre-processing steps are
            performed by the \verb~cropsquare~ and the
            \verb~generatefeaturevector~ methods in the code. Intermediate
            results from these steps can be seen by setting the
            \verb~writeintermediate~ parameter in the code to \verb~True~, or in
            Figure \ref{cropscale}.

            \begin{figure}
                \begin{centering}
                    \includegraphics[width=0.48\columnwidth]{images/lastcropped}
                    \includegraphics[width=0.48\columnwidth]{images/lastscaled}
                \end{centering}
                \caption{Examples of the pizza image (Figure \ref{pizza})
                cropped to square(left) and then scaled to $3 \times 3$ (right).
                The scaled image is than converted to a feature vector which is
                used to locate similar images to generate the photo-mosaic.}
                \label{cropscale}
            \end{figure}
 
        \subsection{Image Description and Categorization}
            Because the generation of a photo-mosaic is essentially a visual
            search through a large database of images, I next developed a
            method of representing the features of the image in a compact and
            rapidly-searchable manner. It was decided that the general
            low-frequency color content is much more import for the generation
            of a photo-mosaic, and due to this, a down-sampled version of each
            image was used to generate the feature vector. For all examples
            later in the report, a $3 \times 3$ image was used as the feature
            vector. The image was then flattened to a $27 \times 1$ vector of
            RGB values and stored in the database. The resulting feature vector for the
            image seen in Figures \ref{pizza} and \ref{cropscale} is the
            following:
            \begin{verbatim}
[7, 7, 7, 5, 5, 5, 125, 82, 31, 13, 12, 10, 61, 59, 18, 95, 86, 19,
36, 22, 11, 98, 95, 16, 105, 100, 18]
            \end{verbatim}
        \subsection{Image Database}
            Next, we generate a matrix of vectors. Using the feature vector
            described above, this is a
            $27 \times n$ matrix, where $n$ is the number of images in the
            database, and each row contains a feature vector. The matrix of
            feature vectors is then used to generate an index using the flann
            library. Also, a list of images in the database and their location
            on the disk is saved along with the index. This allows for quick
            location of the original file using the results of a search.

            For the images used, it took a few minutes to generate this
            database, so the capability to save the database index to disk was
            added so that the database wouldn't have to be rebuilt every time
            the software is run to allow for rapid experimentation and
            development.

        \subsection{Matching and Final Image Generation}
            Next, when supplied with a target image, the target image is
            pre-processed in preparation to generate the mosaic. Given a number
            of parameters such as the number of tiles and the tile size in
            pixels, the target image is re-sized and an empty final image of the
            appropriate size is initialized. The re-sized target image is then
            broken into $3 \times 3$ pixel blocks. These blocks are then passed
            to the database index. When an approximate match has been returned,
            the image is located on disc, cropped to square, re-sized to the
            tile size provided by the user, and then ``pasted'' into the final image
            using Pygame's \verb~blit~ function. As you can see, none of the
            images on the user's computer are affected by this process, except
            the final output image which is saved after all tiles have been
            ``pasted'' in.

    \section{Work Performed}
        For this project I wrote python code that performs the approach outlined
        under ``Approach''. I tested the code with a number of target images and
        a database of approximately 4100 tile images.
    \section{Results}
        Sample results can be seen in Figures \ref{pmosaic} and \ref{pmosaic2}.
        \begin{sidewaysfigure}
            \includegraphics[width=\columnwidth]{images/pizzamosaic}
            \caption{Mosaic generated from the database using 4100 images in the
            tile database.}
            \label{pmosaic}
        \end{sidewaysfigure}
        \begin{sidewaysfigure}
            \includegraphics[width=\columnwidth]{images/artinstmosaic}
            \caption{Mosaic generated from the database using 4100 images in the
            tile database.}
            \label{pmosaic2}
        \end{sidewaysfigure}
        Each of these images took less than twenty minutes to generate, and they
        are each $128 \times 171$ mosaic tiles in size, meaning nearly 22000
        database queries were made. In order to analyze the time spent on each
        task to provide guidance for future optimization, the program was
        profiled during the generation of Figure \ref{pmosaic}.  The results of
        this profiling show that most of the time spent while generating a
        mosaic was spent loading and scaling images (Figure \ref{profiling}).
        Interestingly, this opens the opportunity to some improvements because
        it indicates that the feature search was relatively fast, and could be
        made more complex to capture more features which may improve the final
        result.
        \begin{figure}
            \includegraphics[width=\columnwidth]{images/profileresults}
            \caption{Results of profiling the generation of the mosaic seen in
            Figure \ref{pmosaic} detailing the amount of time spent doing each
            task. This makes it clear that the querying of the database is only
            a tiny fraction of the time spent generating a mosaic, and that much
            of the time was spent loading and scaling images.}
            \label{profiling}
        \end{figure}
    \section{Discussion}
        Overall, using flann as a means to match features of images to a
        database can be considered a success. My program succeeds in generating
        recognizable mosaics in a reasonable amount of time, entirely automatically.
        Although the program achieves what it set out to do, in the development
        and execution of this project I learned a number of things that could be
        done to improve the function of the program, and which may be
        investigated at a later date. Firstly, there is a lot of duplicated
        image tiles in both of the result images. Although this seems obvious
        now, if you generate a photo-mosaic using 22000 tiles, it would be
        beneficial to use a database of significantly more than 22000 images.
        This would help to guarantee that when a search is performed for a
        certain feature vector that it is more likely that a tile image exists
        that is close to the feature vector, which would reduce the duplication
        visible in the resultant images. To me at least, the result in Figure
        \ref{pmosaic2} is significantly better in appearance than that of Figure
        \ref{pmosaic}. I believe this is due to the fact that there are more
        neutral colored images in my photo library than brightly colored ones.
        This means that many details in \ref{pmosaic2} were surprisingly well
        captured (for example, the brick lines on the first floor) because there
        was an image with a closer match simply due to the fact that there are
        more images with a light or gray color to choose from.
        
        Secondly, it would be beneficial to investigate the use of a larger
        feature vector, which, at the cost of higher storage and
        processing time, would allow the software to generate more accurate and
        believable photo-mosaics given enough source images. As detailed in the
        results section, profiling of the program indicated that disc I/O was
        a significantly higher workload than database look-up, so implementing a
        more complex feature vector would be appropriate to improve the quality
        of the images without too much processing penalty.

        Thirdly, I did not tweak any of the parameters used for the flann library
        which performed the feature matching. It is possible to tailor match
        tolerances, algorithms used, and to provide custom distance measures to
        the algorithm, all of which might greatly improve the quality of the
        resulting images and perhaps optimize the speed of the database.
       
        In all, I am happy to say that the developed software accomplishes the
        goals that it intends to, and that I have demonstrated a robust,
        automatic, and fast photo-mosaic generating software.
    \bibliography{report}{}
    \bibliographystyle{IEEEtranM}
\end{document}
